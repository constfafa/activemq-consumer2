package cn.bellychang.service;

import cn.bellychang.model.Email;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.concurrent.TimeUnit;

/**
 * @author ChangLiang
 * @date 2018/5/29
 */
@Service
public class Receiver {

    @JmsListener(destination = "${activemq.queueName}",containerFactory = "jmsListenerContainerQueue")
    public void recievedQueueMessage(Email email) throws InterruptedException {
        System.out.println("Recieved queue Message: " + email.toString());
    }

    @JmsListener(destination = "${activemq.topicName}",containerFactory = "jmsListenerContainerTopic")
    public void recievedTopicMessage(Email email){
        System.out.println("Recieved topic Message: " + email.toString());
    }
}
